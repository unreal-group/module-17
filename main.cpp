#include <iostream>
#include <cmath>

class Vector {
public:
    Vector() = default;

    Vector(float x, float y, float z) : x(x), y(y), z(z) {}

    float GetX() {
        return x;
    }

    float GetY() {
        return y;
    }

    float GetZ() {
        return z;
    }

    float calculateModule() {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

private:
    float x = 1.0;
    float y = 2.0;
    float z = 3.0;
};

void printInfo(std::string vectorName, Vector vector) {
    std::cout << vectorName << ":" << std::endl;
    std::cout << "X = " << vector.GetX() << std::endl;
    std::cout << "Y = " << vector.GetY() << std::endl;
    std::cout << "Z = " << vector.GetZ() << std::endl;
    std::cout << "Module = " << vector.calculateModule() << std::endl;
}

int main() {
    printInfo("Default", Vector());
    std::cout << "=====================" << std::endl;
    printInfo("Custom", Vector(25, 13, 22));
    return 0;
}
